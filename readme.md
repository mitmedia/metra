Metra rail’s app
==============
Offers line and route information.


Installation Steps:
------

- Clone project
- Checkout dev branch
- Create your branch (topics/feature)
- Run `npm install`
- Run `bower install`
- Run `npm run dev`
- Run `gulp watch` (another tab)
- Run `compass watch` (another tab)

NOTES:
------

- HTML and JS editable files are in "src" folder;
- SASS files are in "compass_components";

